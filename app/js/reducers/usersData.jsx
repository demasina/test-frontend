export default function() {
	return [
		{
			id:1,
			name:"Kirill",
			email:"v@mail.ru",
			gender:"male",
			img: "../../sass/images/мужик.png" 
		},
		{
			id:2,
			name:"Sasha",
			email:"s@mail.ru",
			gender:"female",
			img: "../../sass/images/женщина.png" 
		},
		{
			id:3,
			name:"Artem",
			email:"a@mail.ru",
			gender:"male",
			img: "../../sass/images/мужик.png" 
		}
	]
}