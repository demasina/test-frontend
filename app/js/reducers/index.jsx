import {combineReducers} from 'redux';
import UsersReducers from './usersData.jsx';
import ActiveUser from './userActive.jsx';

const allReducers = combineReducers({
    users: UsersReducers,
    active: ActiveUser
})

export default allReducers;