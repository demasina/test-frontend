import React from 'react';
import ReactDOM from 'react-dom';
import { render } from 'react-dom';
import App from './components/App';
import '../sass/styles.scss';
import { createStore } from 'redux';
import {Provider} from 'react-redux';
import allReducers from './reducers/index.jsx';

const storage = createStore(allReducers);

render(
  <Provider store={storage}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
