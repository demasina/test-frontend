export const SelectUser = (user) => {
    return {
        type: "USER_SELECTED",
        payload: user
    }
}