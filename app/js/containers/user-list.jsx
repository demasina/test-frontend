import React, {Component} from 'react';
import { connect } from 'react-redux';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';


class UsersList extends Component {
    show() {
        console.log(this.props.users)
        return this.props.users.map((user) => {
            return (
                <tbody key={user.id}>
                    <tr>
                        <td>{user.name}</td>
                        <td>{user.gender}</td>
                        <td>{user.email}</td>
                        <td><img src={user.img} alt=""/></td>
                        <td><Link to="/update/{key}"><button className="but ed">Edit</button></Link><button className="but rem">Remove</button></td>
                    </tr>
                </tbody>
            )
        })
    }
    render() {
        return(
            <table>

                {this.show()}

            </table>

        )
    }
}
function mapStateToProps(state) {
    return {
        users: state.users
    }
}
export default connect(mapStateToProps) (UsersList);