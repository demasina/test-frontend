import React from "react";
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import UsersList from '../containers/user-list.jsx';
import allReducers from "../reducers";
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import Update from '../containers/Update';
import { browserHistory } from 'react-router';


const store = createStore(allReducers)

export default function App() {
    return (
        <Provider store={ store }>

        <Router onUpdate={() => window.scrollTo(0, 0)} history={ browserHistory }>
            <div>
                <Route path='/' component={ UsersList } />
                {/* <Route path='/:create' component={ HomePage } /> */}
                <Route path='/update/{key}' component={ Update } />

            </div>
        </Router>

    </Provider>
    );
  }
  